import React, { useState, useContext } from 'react'
import { CustomContext } from '../components/Context';
import { Modal } from "../components/Modal";
import { Button } from "../components/Button";

const Cart = () => {

  const {cart, delCart} = useContext(CustomContext)
  const [showModal, setShowModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);

  const handleDeleteButtonClick = (product) => {
    setSelectedProduct(product);
    setShowModal(true);
  };

  const handleOkButtonClick = () => {
    if (selectedProduct) {
      delCart(selectedProduct.id);
      setSelectedProduct(null);
      setShowModal(false);
    }
  };

  const handleCloseModal = () => {
    setSelectedProduct(null);
    setShowModal(false);
  };


  return (
    <section className="cart-products">
      <div className="container">
        <h1 className="cart-products__header">Кошик</h1>
        <div className="cards-products__cards">
          {cart.map((item) => (
            <article
              key={item.id}
              className="cards-products__card card-product"
            >
              <div className="card-product__image-box">
                <img src={item.url} alt={item.name} />
              </div>
              <div className="card-product__details">
                <h4 className="card-product__name">{item.name}</h4>
                <p className="card-product__color">{item.color}</p>
                <div className="card-product__buy-block">
                  <p className="card-product__price">{item.price}$</p>
                  <button onClick={() => handleDeleteButtonClick(item)}>
                    Видалити
                  </button>
                </div>
              </div>
            </article>
          ))}
        </div>
        <button className="cart-products__all-buy">Оформити заказ</button>
      </div>
      {showModal && (
        <Modal
          header="Видалити товар з кошика?"
          closeButton={true}
          text=""
          actions={
            <>
              <Button
                backgroundColor="black"
                text="Так"
                onClick={handleOkButtonClick}
              />
              <Button
                backgroundColor="black"
                text="Ні"
                onClick={handleCloseModal}
              />
            </>
          }
          onClose={handleCloseModal}
        />
      )}
    </section>
  );
}

export { Cart }