import React, { useContext } from "react";
import { CustomContext } from "../components/Context";
import { FaStar } from "react-icons/fa";

const Favorites = () => {

  const { favorites, delFavorites } = useContext(CustomContext);

  return (
    <section className="cart-products">
      <div className="container">
        <h1 className="cart-products__header">Обране</h1>
        <div className="cards-products__cards">
          {favorites.map((item) => (
            <article
              key={item.id}
              className="cards-products__card card-product"
            >
              <div className="card-product__image-box">
                <img src={item.url} alt={item.name} />
              </div>
              <div className="card-product__details">
                <h4 className="card-product__name">{item.name}</h4>
                <p className="card-product__color">{item.color}</p>
                <div className="card-product__buy-block">
                  <p className="card-product__price">{item.price}$</p>
                  <FaStar
                    className='card-product__favorite-icon'
                    onClick={() => delFavorites(item.id)}
                  />
                </div>
              </div>
            </article>
          ))}
        </div>
      </div>
    </section>
  );
}

export { Favorites }