import React, { useContext, useState } from 'react'
import { CustomContext } from './Context';
import { Modal } from './Modal';
import { Button } from './Button';
import { FaStar } from "react-icons/fa";

const Cards = ({ products }) => {

    const {
      addCart,
      cart,
      favorites,
      handleAddToFavorites,
    } = useContext(CustomContext);

    const [showModal, setShowModal] = useState(false);
    const [selectedProduct, setSelectedProduct] = useState(null); 

    const handleAddToCart = (product) => {
      setSelectedProduct(product);
      setShowModal(true);
    };

  const handleOkButtonClick = () => {
    if (selectedProduct) {
      addCart(selectedProduct);
      setSelectedProduct(null);
      setShowModal(false);
    }
  };

  const handleCloseModal = () => {
    setSelectedProduct(null);
    setShowModal(false);
  };

  return (
    <section className="cards-products">
      <div className="container">
        <h2 className="cards-products__title">Виберіть ноутбук</h2>
        <div className="cards-products__cards">
          {products &&
            products.map((product) => (
              <article
                key={product.id}
                className="cards-products__card card-product"
              >
                <div className="card-product__image-box">
                  <img src={product.url} alt={product.name} />
                </div>
                <div className="card-product__details">
                  <h4 className="card-product__name">{product.name}</h4>
                  <p className="card-product__color">{product.color}</p>
                  <div className="card-product__buy-block">
                    <p className="card-product__price">{product.price}$</p>
                    <FaStar
                      className={`card-product__favorite-icon ${
                        favorites.includes(product) ? "selected" : ""
                      }`}
                      onClick={() => handleAddToFavorites(product)}
                    />
                    {cart.findIndex((item) => item.id === product.id) > -1 ? (
                      <button className="card-product__add-button card-product__add-button--inactive">
                        Product added
                      </button>
                    ) : (
                      <button
                        className="card-product__add-button"
                        onClick={() => handleAddToCart(product)}
                      >
                        Add to cart
                      </button>
                    )}
                  </div>
                </div>
              </article>
            ))}
        </div>
      </div>
      {showModal && (
        <Modal
          header="Додати товар в кошик?"
          closeButton={true}
          text=""
          actions={
            <>
              <Button
                backgroundColor="black"
                text="Так"
                onClick={handleOkButtonClick}
              />
              <Button
                backgroundColor="black"
                text="Ні"
                onClick={handleCloseModal}
              />
            </>
          }
          onClose={handleCloseModal}
        />
      )}
    </section>
  );
}

export { Cards }