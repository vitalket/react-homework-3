import React from "react";

const Modal = ({ header, closeButton, text, actions, onClose }) => {
  return (
    <div className="modal-overlay" onClick={onClose}>
      <div className="modal" onClick={(e) => e.stopPropagation()}>
        {closeButton && (
          <span className="close-button" onClick={onClose}>
            &times;
          </span>
        )}
        <div className="modal-header">{header}</div>
        <div className="modal-content">{text}</div>
        <div className="modal-actions">{actions}</div>
      </div>
    </div>
  );
};

export { Modal }
