import React, { createContext, useState, useEffect } from 'react';

export const CustomContext = createContext();

export const Context = ({ children }) => {

  const [cart, setCart] = useState([]);
  const [favorites, setFavorites] = useState([]);

  useEffect(() => {
    const storedCart = JSON.parse(localStorage.getItem("cart"));
    const storedFavorites = JSON.parse(localStorage.getItem("favorites"));
    if (storedCart !== null) {
      setCart(storedCart);
    }
    if (storedFavorites !== null) {
      setFavorites(storedFavorites);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
    localStorage.setItem("favorites", JSON.stringify(favorites));
  }, [cart, favorites]);

 const addCart = (product) => {
    setCart((prev) => [...prev, product]);
  };

  const delCart = (id) => {
    setCart((prev) => prev.filter(item => item.id !== id))
  }

  const handleAddToFavorites = (product) => {
    setFavorites(
      favorites.includes(product)
        ? favorites.filter((fav) => fav !== product)
        : [...favorites, product]
    );
  };

  const delFavorites = (id) => {
    setFavorites((prev) => prev.filter((item) => item.id !== id));
  };

  const value = {
    cart,
    addCart,
    delCart,
    handleAddToFavorites,
    delFavorites,
    favorites,
  };

  return (
    <CustomContext.Provider value={value}>{children}</CustomContext.Provider>
  );
}

