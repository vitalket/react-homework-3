import React, { useContext } from 'react';
import { Link, Outlet } from 'react-router-dom';
import { FaShoppingCart, FaStar } from "react-icons/fa";
import { CustomContext } from './Context';

const Layout = () => {
    const { cart, favorites } = useContext(CustomContext)

  return (
    <>
      <header className="header">
        <div className="container">
          <div className="header__block">
            <div>LOGO</div>
            <nav>
              <ul>
                <li>
                  <Link to="/">Home</Link>
                </li>
              </ul>
            </nav>
            <div className="header__icons">
              <Link to="/cart">
                <FaShoppingCart />
                <span>{cart.length}</span>
              </Link>
              <Link to="/favorite">
                <FaStar />
                <span>{favorites.length}</span>
              </Link>
            </div>
          </div>
        </div>
      </header>

      <main className="main">
        <Outlet />
      </main>

      <footer className="footer">
        <div className="container">Footer</div>
      </footer>
    </>
  );
}

export { Layout }