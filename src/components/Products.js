import React, { useState, useEffect } from "react";
import { Cards } from "./Cards";

const Products = () => {
  const [items, setItems] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    const getProducts = async () => {
      try {
        const response = await fetch("products.json");

        if (!response.ok) {
          throw new Error("Failed to fetch products");
        }

        const products = await response.json();

        const defaultValues = {
          name: "Laptop",
          color: "Black",
          price: 0,
          url: "Default URL",
        };

        const productsWithDefaults = products.map((product) => ({
          ...defaultValues,
          ...product,
        }));

        setItems(productsWithDefaults);
      } catch (error) {
        setError(error.message);
      }
    };

    getProducts();
  }, []);

  return <>{error ? <p>Error: {error}</p> : <Cards products={items} />}</>;
};

export { Products }
