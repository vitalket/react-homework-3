import React from "react";

function Button({ backgroundColor, text, onClick }) {
  return (
    <button
      className="open-modal-button"
      style={{ backgroundColor }}
      onClick={onClick}
    >
      {text}
    </button>
  );
}

export { Button }
